#include "inanimate.h"
#include "entity.h"
#include "game.h"


Inanimate::Inanimate() {

}

Inanimate::~Inanimate() {

}

Direction Inanimate::getMoveDirection(Game *game, Entity *entity) {
  (void)game;
  (void)entity;
  return Direction::NONE;
}

bool Inanimate::isUser() const {
  return false;
}
