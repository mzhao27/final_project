#include "game.h"
#include "maze.h"
#include "position.h"
#include "entity.h"
#include "ecfactory.h"
#include "basicgamerules.h"
#include "gamerules.h"
#include <vector>
#include <string>

using std::cout;
using std::endl;

Game::Game() {
  this->entityVec = new EntityVec;
  this->TUI = nullptr;
}

Game::~Game() {
  delete this->currMaze;
  if (this->TUI)
     delete this->TUI;
  delete this->rules;
  delete this->entityVec;

  this->TUI = nullptr;
}

void Game::setMaze(Maze *maze) {
  this->currMaze = maze;
}

void Game::setGameRules(GameRules *gameRules) {
  this->rules = (BasicGameRules*)gameRules;

}

void Game::setUI(TextUI *ui) {
  this->TUI = ui;
}

void Game::addEntity(Entity *entity) {
  this->entityVec->push_back(entity);
}

Entity* Game::getEntityAt(const Position &pos) {
  // return m if it is in pos
  std::vector<Entity *> minotaurs = getEntitiesWithProperty('m');
  for (auto m : minotaurs) {
    if (m->getPosition() == pos)
      return m;
  }
  for (std::vector<Entity*>::iterator i = this->entityVec->begin(); i != this->entityVec->end(); ++i) {
    Entity *curr = *i;
    Position currPos = curr->getPosition();
    if (pos == currPos) {
      return curr;
    }
  }
  return nullptr;
}

const Game::EntityVec& Game::getEntities() const {
  return *this->entityVec;
}

std::vector<Entity*> Game::getEntitiesWithProperty(char prop) const {
  std::vector<Entity*> toReturn;
  for (auto i = this->entityVec->begin(); i != this->entityVec->end(); ++i) {
    Entity *curr = *i;
    std::string props = curr->getProperties();
    if (prop == props[0]) {
      toReturn.push_back(curr);
    }
  }
  return toReturn;
}

Maze* Game::getMaze() {
  return this->currMaze;
}

UI* Game::getUI() {
  return this->TUI;
}

GameRules* Game::getGameRules() {
  return (GameRules*)this->rules;
}

void Game::gameLoop() {
  //prints the game out and shows when hero wins/loses
  GameResult res = this->rules->checkGameResult(this);
  while (res == GameResult::UNKNOWN) {
    //    this->TUI->render(this);
    for (std::vector<Entity*>::iterator i = entityVec->begin(); i != entityVec->end(); ++i) {
      Entity *currEntity = *i;
      if (currEntity->getController()->isUser()) {
        this->TUI->render(this);
      }
      this->takeTurn(currEntity);
      res = this->rules->checkGameResult(this);
      if (res == GameResult::HERO_WINS) {
        this->TUI->render(this);
        this->TUI->displayMessage("Hero wins", true);
	break;
      }
      if (res == GameResult::HERO_LOSES) {
        this->TUI->render(this);
        this->TUI->displayMessage("Hero loses", true);
	break;
      }
    }
  }

}

void Game::takeTurn(Entity *actor)  {
  //moves the direction of the entitycontroller
  EntityController *ec = actor->getController();
  Direction dir = ec->getMoveDirection(this, actor);
  actor->setPosition(actor->getPosition().displace(dir));
  // cout << actor->getGlyph() << ": " << actor->getPosition() << endl;
/*
  Position source = actor->getPosition();
  Position dest;
  switch (dir) {
    case Direction::LEFT:
      dest = Position(source.getX() - 1, source.getY());
      break;
    case Direction::RIGHT:
      dest = Position(source.getX() + 1, source.getY());
      break;
    case Direction::UP:
      dest = Position(source.getX(), source.getY() + 1);
      break;
    case Direction::DOWN:
      dest = Position(source.getX(), source.getY() - 1);
      break;
    default:
      if (actor->getController()->isUser()) {
        this->TUI->displayMessage("Illegal Move", false);
      }
      break;
  }
  this->rules->enactMove(this, actor, dest);
*/
}

Game* Game::loadGame(std::istream &in) {
  Maze *maze = Maze::read(in);
  if (!maze) {
    return nullptr;
  }
  Game *game = new Game();
  game->setMaze(maze);
  std::string description;
  int x, y;
  //error checking
  while (in >> description >> x >> y) {
    Entity *curr = new Entity();
    if (description.length() > 3 || x < 0 || y < 0) {
      delete game->entityVec;
      return nullptr;
    }
    //sets glyph and property to the first and third character
    curr->setGlyph(description.substr(0,1));
    curr->setProperties(description.substr(2,1));
    EntityControllerFactory *ecfactory = EntityControllerFactory::getInstance();
    curr->setController(ecfactory->createFromChar(description[1]));

    Position currPos(x, y);
    curr->setPosition(currPos);
    game->addEntity(curr);
  }

  return game;

}
