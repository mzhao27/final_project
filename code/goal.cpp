#include "goal.h"
#include <string>
#include "position.h"

using std::string;

Goal::Goal() {
}

Goal::~Goal() {
}

MoveResult Goal::checkMoveOnto(Entity *, const Position &fromPos, const Position &tilePos) const {

  if (fromPos.distanceFrom(tilePos) != 1) {
    return MoveResult::BLOCK;
  }
  return MoveResult::ALLOW;
}

bool Goal::isGoal() const {
  return true;
}

string Goal::getGlyph() const {
  return "<";
}
