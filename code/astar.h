A#ifndef ASTAR_H
#define ASTAR_H

#include "position.h"

class AStar : public Position {
private:
  struct aStarNode{
    Position start;
    struct List *next;
  };
  aStarNode *head;
  
public:
  AStar();
  virtual ~AStar();
  virtual bool isWall();
  
};

#endif // ASTAR_H 
