#include "maze.h"
#include "tilefactory.h"
#include "tile.h"
#include "position.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using std::string;
using std::vector;

typedef vector <Tile*>::iterator itr;
Maze::Maze(int width, int height): m_width(width), m_height(height) {
  int len = width * height;
  for (int i = 0; i < len; i++) {
    m_tiles.push_back(nullptr);
  }
}

Maze::~Maze() {
  /*****
  int length = this->height * this->width;
  for (int i = 0; i < length; i++) {
  if (this->tileVec[i] != nullptr)
    delete &(this->tileVec[i]);
  }
  ****/
  for (itr it = m_tiles.begin(); it != m_tiles.end(); it++){
    if (*it != nullptr)
      delete *it;
  }
  }

int Maze::getWidth() const {
  return m_width;
}

int Maze::getHeight() const {
  return m_height;
}

bool Maze::inBounds(const Position &pos) const {
  if (pos.getX() >= 0 && pos.getY() >= 0 && pos.getX() < m_width && pos.getY() < m_height)
    return true;
  else
    return false;
}

int Maze::posToInt(const Position &pos) const {
  return (this->m_width * pos.getY()) + pos.getX();
}

void Maze::setTile(const Position &pos, Tile *tile) {
  int index = this->posToInt(pos);
  this->m_tiles[index] = tile;
}


const Tile* Maze::getTile(const Position &pos) const {
  int index = this->posToInt(pos);
  return this->m_tiles[index];

}

Maze* Maze::read(std::istream &in) {
  int x,y;
  string currLine;
  //in >> x
  //in >> y;
  std::getline(in, currLine);
  std::stringstream ss(currLine);
  int num;
  std::vector<int> dimNumbers;
  while (ss >> num) {
    dimNumbers.push_back(num);
  }
  if (dimNumbers.size() != 2) {
    return nullptr;
  }
  x = dimNumbers[0];
  y = dimNumbers[1];
  if (x <= 0 || y <= 0) {
    return nullptr;
  }
  Maze* maze = new Maze(x, y);
  //string currLine;
  char currGlyph;
  TileFactory *tf = TileFactory::getInstance();
  int j;
  for (j = 0; j < y; j++) {
    std::getline(in, currLine);
    // in >> currLine;
    if ((int)currLine.length() != x) {
      delete maze;
      return nullptr;
    }
    for (int i = 0; i < x; i++) {
      currGlyph = currLine[i];
      if (currGlyph != '#' && currGlyph != '.' && currGlyph != '<' && currGlyph != '?') {
        delete maze;
        return nullptr;
      }
      Position pos(i, j);
      Position& refPos = pos;
      Tile *currTile = tf->createFromChar(currGlyph);
      maze->setTile(refPos, currTile);
    }
  }
  return maze;

}
