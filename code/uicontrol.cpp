#include "uicontrol.h"
#include "game.h"
#include "basicgamerules.h"
#include "entity.h"
#include "position.h"
using std::cout;
using std::endl;
using std::cin;

UIControl::UIControl() {

}

UIControl::~UIControl() {

}

Direction UIControl::getMoveDirection(Game *game, Entity *entity) {
  cout << "Your move (u/d/l/r) ";
  char move;
  cin >> move;
  while (move !='u' && move != 'd' && move != 'l' && move != 'r') {
    cout << "Unknown direction" << endl;
    //this->displayMessage("Unknown direction", false);
    cin >> move;
  }
  Direction dir = Direction::NONE;
  switch (move) {
    case 'u':
      dir = Direction::UP;
      break;
    case 'd':
      dir = Direction::DOWN;
      break;
    case 'l':
      dir = Direction::LEFT;
      break;
    case 'r':
      dir = Direction::RIGHT;
      break;
    default:
      break;
  }
  Position epos = entity->getPosition();
  if (game->getGameRules()->allowMove(game, entity, epos, epos.displace(dir))) {
    return dir;
  }
  
  return Direction::NONE;
}

bool UIControl::isUser() const {
  return true;
}
