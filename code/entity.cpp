
#include "entity.h"

Entity::Entity() {

}

Entity::~Entity() {

}

//all getters and setters
void Entity::setGlyph(const std::string &glypha) {
  glyph = glypha;
}

void Entity::setProperties(const std::string &props) {
  this->property = props;
}

std::string Entity::getGlyph() const {
  return glyph;
}


std::string Entity::getProperties() const {
  return property;
}

bool Entity::hasProperty(char prop) const {
  return property.c_str()[0] == prop;
}

void Entity::setController(EntityController *controller) {
  this->controller = controller;
}

EntityController* Entity::getController() {
  return controller;
}

void Entity::setPosition(const Position &pos) {
  position = pos;
}

Position Entity::getPosition() const {
  return position;
}
