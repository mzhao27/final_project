#include "wall.h"
#include <string>

using std::string;

Wall::Wall() {

}

Wall::~Wall() {

}

MoveResult Wall::checkMoveOnto(Entity *, const Position &, const Position &) const {
  return MoveResult::BLOCK;
}

bool Wall::isGoal() const {
  return false;
}

string Wall::getGlyph() const {
  return "#";
}
