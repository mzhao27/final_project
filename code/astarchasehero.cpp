#include "astarchasehero.h"
#include "game.h"
#include "entity.h"
#include "position.h"
#include "tile.h"

void astarAlgorithm();

AStarChaseHero::AStarChaseHero() {

}

AStarChaseHero::~AStarChaseHero() {

}

Direction AStarChaseHero::getMoveDirection(Game *game, Entity *entity) {
   std::vector<Entity *> heroes = game->getEntitiesWithProperty('h');
  int minDist = 100000;
  int dist = 0;
  Entity *target = nullptr;
  for (auto h: heroes) {
    dist = entity->getPosition().distanceFrom(h->getPosition());
    if (minDist > dist) {
      minDist = dist;
      target = h;
    }
  }
  if (target == nullptr) {
    return Direction::NONE;
  }
  Position epos = entity->getPosition();
  int h_diff = target->getPosition().getX() - epos.getX();
  int v_diff = target->getPosition().getY() - epos.getY();
  Direction hdir;
  Direction vdir;
  const Tile *tile;
  if (abs(h_diff) >= abs(v_diff)) {
    if (h_diff == 0) {
      return Direction::NONE;
    }
    if (h_diff > 0) {
      hdir = Direction::RIGHT;
    }
    else {
      hdir = Direction::LEFT;
    }
    tile = game->getMaze()->getTile(epos.displace(hdir));
    if (tile->checkMoveOnto(entity, epos, epos.displace(hdir)) == MoveResult::ALLOW) {
      return hdir;
    }
    else {
      if (v_diff == 0) {
        return Direction::NONE;
      }
      if (v_diff > 0) {
        vdir = Direction::DOWN;
      }
      else {
        vdir = Direction::UP;
      }
    }
    tile = game->getMaze()->getTile(epos.displace(vdir));
    if (tile->checkMoveOnto(entity, epos, epos.displace(vdir)) == MoveResult::ALLOW) {
      return vdir;
    }
    else {
      return Direction::NONE;
    }
  }
  else {
    if (v_diff > 0) {
      vdir = Direction::DOWN;
    }
    else {
      vdir = Direction::UP;
    }
    tile = game->getMaze()->getTile(epos.displace(vdir));
    if (tile->checkMoveOnto(entity, epos, epos.displace(vdir)) == MoveResult::ALLOW) {
      return vdir;
    }
    else {
      if (h_diff == 0) {
        return Direction::NONE;
      }
      if (h_diff > 0) {
        hdir = Direction::RIGHT;
      }
      else {
        hdir = Direction::LEFT;
      }
    }
    tile = game->getMaze()->getTile(epos.displace(hdir));
    if (tile->checkMoveOnto(entity, epos, epos.displace(hdir)) == MoveResult::ALLOW) {
      return hdir;
    }
    else {
      return Direction::NONE;
    }
  }
  return Direction::NONE;
}

bool AStarChaseHero::isUser() const {
  return false;
}

