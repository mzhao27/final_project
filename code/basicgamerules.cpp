
#include "basicgamerules.h"
#include "tile.h"
#include "entity.h"
#include "position.h"


BasicGameRules::BasicGameRules() {
}

BasicGameRules::~BasicGameRules() {
}

bool BasicGameRules::allowMove(Game *game, Entity *actor, const Position &source, const Position &dest) const {
  if (source.distanceFrom(dest) != 1) {
    return false;
  }
  //moves the tile from source to destination
  const Tile* tile = game->getMaze()->getTile(dest);
  MoveResult r = tile->checkMoveOnto(actor, source, dest);
  if (r == MoveResult::ALLOW) {
    //if the direction moved is not a wall then move it
    Entity *destEntity = game->getEntityAt(dest);
    if (destEntity == nullptr) {
      return true;
    } else if (destEntity->hasProperty('v')) {
      //ENACT TWO MOVES
      int x = dest.getX() + dest.getX() - source.getX();
      int y = dest.getY() + dest.getY() - source.getY();
      Position destv(x, y);
      tile = game->getMaze()->getTile(destv);
      r = tile->checkMoveOnto(actor, dest, destv);
      if (r == MoveResult::BLOCK) {
	//if direction moved is a wall then don't move it
	return false;
      }
      destEntity->setPosition(destv);
      return true;
    }
  }
  return false;
}

void BasicGameRules::enactMove(Game *game, Entity *actor, const Position &dest) const {
  Position source = actor->getPosition();
  //calls allow move to move the entity to new location
  if (this->allowMove(game, actor, source, dest)) {
    actor->setPosition(dest);
  }

}

GameResult BasicGameRules::checkGameResult(Game *game) const {
  //conditions for winning and losing for hero
  std::vector<Entity*> heros = game->getEntitiesWithProperty('h');
  std::vector<Entity*> minotaurs = game->getEntitiesWithProperty('m');
  Maze *maze = game->getMaze();
  for (std::vector<Entity*>::iterator i = heros.begin(); i != heros.end(); ++i) {
    Entity *currHero = *i;
    Position hPos = currHero->getPosition();
    const Tile *hTile = maze->getTile(hPos);
    if (hTile->isGoal()) {
      return GameResult::HERO_WINS;
    }
  }

  for (std::vector<Entity*>::iterator i = minotaurs.begin(); i != minotaurs.end(); ++i) {
    for (std::vector<Entity*>::iterator j = heros.begin(); j != heros.end(); ++j) {
      Entity *currH = *j;
      Entity *currM = *i;
      Position hPos = currH->getPosition();
      Position mPos = currM->getPosition();
      if (hPos == mPos) {
        return GameResult::HERO_LOSES;
      }
    }
  }


  return GameResult::UNKNOWN;
}
