commit 0c76c537fff82e7adf3dc0533868a32a9d2614f1
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Wed May 6 18:29:50 2020 -0400

    final edits

commit 45121ec463e9b44f3df753b5a2fb8e862323e59c
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Wed May 6 18:12:45 2020 -0400

    added comments

commit 15f89cc7902898a101dadd6782b298429278e2f6
Merge: b7617fb 54882b4
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Wed May 6 17:09:20 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit b7617fb406fbdc870a5702e884111ac1ff40dd16
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Wed May 6 17:08:57 2020 -0400

    copied chasehero

commit 54882b422a31e82aa247fc01f2c3ff9330ffc82d
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Wed May 6 10:11:07 2020 -0400

    added readme

commit b41b368c6e6b1ad4d957f49d4af9ef2b07bb3f27
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Wed May 6 01:45:27 2020 -0400

    fixed compiler warnings

commit a02060d4693b58bd71e8b79a4a9a3860714ff78e
Merge: 73d6ec2 0f166e4
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Wed May 6 01:27:27 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 0f166e423fa2d29fcb106c4effa79d2803bf4a2a
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Wed May 6 00:44:03 2020 -0400

    get entity

commit 42339d148e21d4a1d2a6a7821501ae12acb3b899
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Tue May 5 23:56:33 2020 -0400

    handled movable

commit 73d6ec20bfe0cd66fc5aa37f3b244b7f67e123ee
Merge: 70fe47b 4dc9286
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Tue May 5 23:29:12 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 4dc9286d9ff2f7a70a095df3e256f83356a12903
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Tue May 5 20:38:38 2020 -0400

    onlyone

commit ce2906df81e6f6efefda806014fe883b2acc4dc1
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue May 5 20:17:37 2020 -0400

    add asterick

commit 4fc683b7e8fae4b346a6a495ddb6e8ffaa2a4990
Merge: 7564d4c b758c7b
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue May 5 12:45:35 2020 -0400

    clear
    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 7564d4cdcc76111d92c237a618e3a166d9a67043
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue May 5 12:45:22 2020 -0400

    minor edit

commit 70fe47bc1d621c7dd4678d9a6c9cd88063aeb81f
Merge: 16f2350 b758c7b
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Tue May 5 04:31:43 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit b758c7b86e370bd50ce64eb84a75cd0e1af87c82
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Tue May 5 01:53:13 2020 -0400

    fixed wiinning

commit 16f235061470797bfe4bd149ebe0491ca9024d68
Merge: 430af1e 0acc5e3
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Tue May 5 00:49:48 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 0acc5e3d98adfe5af6fe3b87e053acb373c43ffb
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Tue May 5 00:43:43 2020 -0400

    copied stuff from textui to uicontrol

commit 430af1e4d90a208914bc327f391179e3d0960c4d
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Mon May 4 23:55:44 2020 -0400

    astar progress

commit ba25a0d1eef96e663b837ffd68e903dd6a7363ce
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Mon May 4 23:53:24 2020 -0400

    deleted things form astarhero

commit 2baa5df4d85868042ab2b3414a984824aa820658
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:58:13 2020 -0400

    edits to gamge

commit fe6ad8a418f82df8b5b4006bd3e871e2dc388e7f
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:45:20 2020 -0400

    include

commit 96395d8cf7b1cf3bcc318e0a95cd4982a6dd315e
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:43:05 2020 -0400

    msg

commit 0e9e6d2407a1d4e1ca2ea62381236c7540ece9aa
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:22:10 2020 -0400

    extra check in alloMove

commit 1aaf98134b156fa6f7c1771369c249aa04712349
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:18:49 2020 -0400

    changes to TUI

commit 7061bcc831f9ff836c822969c2098ec42fc30aa6
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 17:13:16 2020 -0400

    gameloop

commit d9e1128f8514ab740ef0411f0451e06180b1fab3
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Mon May 4 16:52:47 2020 -0400

    game loop and take turn

commit eb2e9726893b7650b3ca91f729d723e5d76c4442
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Mon May 4 11:40:43 2020 -0400

    worked on astar

commit c97c615ee72db8bd67be7f426813aad4163de5e1
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Mon May 4 03:36:28 2020 -0400

    get moved rection

commit 44cf50cc89270a15357a24075ad094641266ecb4
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Mon May 4 02:15:47 2020 -0400

    did take turn

commit cb8200906ed962792fa45aaf889bea73868bbd80
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sun May 3 22:13:15 2020 -0400

    changes

commit d1dea9bc0d1e0d8292fdbc918222fba82c7e1049
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sun May 3 18:39:46 2020 -0400

    fixex compilaiton errors

commit 0ef86d884f0c40a318ff51ae722309bcf498f3d0
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sun May 3 14:45:54 2020 -0400

    TextUI additions

commit afd948ef7eea1892d9886963af04c4f6cdabc589
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sun May 3 14:28:21 2020 -0400

    checkGameResult first implementation

commit c966693b0005d29dd6a5009202f983e84bda422e
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sun May 3 12:28:16 2020 -0400

    textUI changes

commit df13428064fb21a9c525197833c93344eb67f3fa
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 21:50:59 2020 -0400

    trying to compile gametest

commit b8575d4dffcbad1addd49bb1f58d87e937c96cd7
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 21:47:48 2020 -0400

    edits to TextUI

commit 869832fc4297b56901c40477e5340a257e7c1196
Merge: bd3d17e 0306fc9
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 21:38:22 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit bd3d17edefafbb6d634feec78f7c3be3071140dd
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 21:38:06 2020 -0400

    additions to game and textUI

commit 0306fc928fe36aa7f3e2ce78cd9c278fc3f612cb
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 19:37:40 2020 -0400

    fixed error statemetn

commit d64ef7bc27003373f8784dca5d8b6505169fe485
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 19:02:36 2020 -0400

    error conditions

commit 2cd5d66126a9b10ec4247f5f2193f8cd5805632c
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 18:19:14 2020 -0400

    entity functionality for game

commit e82c4152aa05f0f8b0c9d2558d17c9b7c5306c6e
Merge: 49f53c4 3e30ed5
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 16:59:01 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 49f53c49c81e07aacc55bcb29690a315ebf2bf34
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 16:58:50 2020 -0400

    more edits to game

commit 3e30ed57468e7bdd7632a9af8dac37e153e1ad75
Merge: 8ff0937 94b028c
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 16:28:08 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit 8ff0937aba6e087e632034ab66b1f092b4591376
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 16:27:55 2020 -0400

    fixdx dimenios

commit 94b028c7a11a5b8f80656d1b8b0193b302ebcf57
Merge: c6277f4 6f37743
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 15:43:22 2020 -0400

    new edits

commit c6277f4fdf96b424a322ace69e54ac22592bf524
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 15:42:36 2020 -0400

    further edits

commit 6f37743b5a49284c16b05f5f971818337ed7b7b6
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 11:48:23 2020 -0400

    modified maze function

commit d4f1ebd78b965f036d89b8e02b0e41fa896d4931
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 03:17:39 2020 -0400

    fixed entity

commit cb73a0d72513eb205981c09b0c64c9851956a819
Merge: f8afa13 b2f5fb7
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 02:55:06 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project
    
    added files

commit f8afa1364b84bff23bda003b380594bcca9e4682
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat May 2 02:51:02 2020 -0400

    added some files and fixed compile

commit b2f5fb70026aeceed767bc61046699fcbd2ec22e
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 02:09:10 2020 -0400

    added file

commit 9203b483d4847fd2090aa734e21a023be5f25317
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 00:33:02 2020 -0400

    updated Makefile

commit fc750b134ef944daeefe82f2f7eb3d99be33e6a6
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 00:16:36 2020 -0400

    finished entity

commit 7cc08188182d15fad502236d2b567352ace8ffa4
Merge: a5c6e2b 03cd7cf
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 00:14:10 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit a5c6e2b89760a6e4e14cb4f5a87e745b15d86b8a
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Sat May 2 00:13:44 2020 -0400

    adding tests

commit 03cd7cf93e4d1902b3be9f04721c6ba14d3015e3
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat May 2 00:01:55 2020 -0400

    new files/edits to maze

commit 5de29be755b50764cd5590774d4908f0f4ae5243
Author: Ryunosuke <rsaito1@jh.edu>
Date:   Fri May 1 23:13:17 2020 -0400

    testing

commit 3080eedd7f74f16ef72330b13982d3be991d4bc6
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sun Apr 26 01:48:28 2020 -0400

    implemented some entitty

commit a7f07a58eee880c5f53e403124920a5604fd7efd
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:42:56 2020 -0400

    variables

commit b8ff01bf175e993d24b75a978899466c108468f0
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:15:13 2020 -0400

    std

commit 3f64e88a047c33e77c5334d6dc278a0ad3864d28
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:14:09 2020 -0400

    using

commit 178d2a03c2a006401e2e815abe8372bf578d136c
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:12:42 2020 -0400

    compile errors

commit 6dae062e221ddfc31a9ab3cc0f34a11d9f2e20d9
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:10:35 2020 -0400

    compiler errors

commit 38477cf7b5ac7f2d636cff8628e98b213badac9e
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 21:02:35 2020 -0400

    fixed compile errors

commit ed942398207901832aca8a42178a451cf39f69bb
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Sat Apr 25 20:33:06 2020 -0400

    fixed compile errors

commit 7efef5ee3ac42b727def58f709ab47d7c373d26c
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 23:20:25 2020 -0400

    changes to game

commit 7423f760a428577a710107b4551a60d87b1fd00d
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 23:16:45 2020 -0400

    destructor for maze

commit 05c9ee2f2b2ff2fccb89233222aa78a8f59b2ff5
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 23:11:23 2020 -0400

    destructor for maze

commit 2e78015a80cabb31dd9c28a15be27c9ad40f6b4d
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 23:07:42 2020 -0400

    initial implementation of Maze.cpp

commit e3d9ea621e0d8769ae3eb3fc7b41314d3e497044
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 22:03:01 2020 -0400

    prototype implementations for reading in files/setting maze

commit 3962178b028afc6d61124bbc19a5e0844c05f29c
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 16:07:57 2020 -0400

    game.cpp skeleton

commit 3a97f05e252fc7656306a484a6f83bd396c139f3
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 14:47:18 2020 -0400

    goal.cpp skeleton

commit 7cd18d46f1f2974f3915e7e956325b504e73021c
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 13:20:11 2020 -0400

    wall.cpp skeleton

commit 093a888e79064a82c3c4d4b7a64469c70d7229b7
Merge: a302e6a 2013200
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 13:07:15 2020 -0400

    Merge branch 'master' of https://bitbucket.org/mzhao27/final_project

commit a302e6a366d5bf31c41dc3003ff329f2909aa309
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Tue Apr 21 13:06:55 2020 -0400

    wall.cpp file made

commit 2013200b1c08ecdaa8d93b0d49182ba063f0bb68
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Tue Apr 21 00:43:54 2020 -0400

    getters and setters

commit 2593187dfc561b9f003702bd84ef123621cf762b
Author: Jamie Huang <jhuan131@jhu.edu>
Date:   Mon Apr 20 19:32:51 2020 -0400

    copied main

commit 81a56051fac74b69a2bc5ec9e18988cf9f41b97a
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sun Apr 19 23:33:57 2020 -0400

    driver

commit 181c38459225700788233fb4222fec3905f1f29f
Author: Matthew Zhao <mzhao26@jhu.edu>
Date:   Sat Apr 18 21:23:12 2020 -0400

    Initial commit, starter code
