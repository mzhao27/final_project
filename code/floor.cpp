#include "floor.h"
#include <string>

using std::string;

Floor::Floor() {
}

Floor::~Floor() {
}

MoveResult Floor::checkMoveOnto(Entity *, const Position &, const Position &) const {
  return MoveResult::ALLOW;
}

bool Floor::isGoal() const {
  return false;
}

string Floor::getGlyph() const {
  return ".";
}
