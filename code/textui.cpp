#include "game.h"
#include "textui.h"
#include "position.h"
#include "maze.h"
#include "tile.h"
#include "entity.h"
#include <iostream>

using std::cout; using std::cin; using std::endl;

TextUI::TextUI(){

}

TextUI::~TextUI(){

}

Direction TextUI::getMoveDirection() {
  this->displayMessage("Your move (u/d/l/r)", false);
  char move;
  cin >> move;
  while (move !='u' || move != 'd' || move != 'l' || move != 'r') {
    //cout << "Unknown direction" << endl;
    this->displayMessage("Unknown direction", false);
    cin >> move;
  }
  switch (move) {
    case 'u':
      return Direction::UP;
      break;
    case 'd':
      return Direction::DOWN;
      break;
    case 'l':
      return Direction::LEFT;
      break;
    case 'r':
      return Direction::RIGHT;
      break;
    default:
      break;
  }
  return Direction::NONE;
}

void TextUI::displayMessage(const std::string &msg, bool endgame) {
  (void) endgame;
  cout << msg << endl;

}

void TextUI::render(Game *game) {
  Maze *maze = game->getMaze();
  int rows = maze->getHeight();
  int cols = maze->getWidth();
  for (int i = 0; i < rows; i++) {
    std::string currLine;
    for (int j = 0; j < cols; j++) {
      Position currPos(j,i);

      const Tile *currTile = maze->getTile(currPos);
      std::string glyph = currTile->getGlyph();
      Entity *e = game->getEntityAt(currPos);
      if (e != nullptr) {
        glyph = e->getGlyph();
      }
      currLine += glyph;
    }
    cout << currLine << endl;
  }
  //TODO: account for multiple entities on one tile
}
